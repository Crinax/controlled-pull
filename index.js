const initPull = (delay) => {
  const pullData = {
    id: undefined,
    start: () => void 0,
    stop: () => void 0,
    fn: () => void 0,
    delay,
  };
  const useDelay = () => pullData.delay;

  pullData.start = () => (pullData.id = setTimeout(function fn() {
    const next = () => (pullData.id = setTimeout(fn, useDelay()));
    pullData.fn(next);
  }, useDelay()));

  pullData.stop = () => (clearTimeout(pullData.id), pullData.id = undefined);

  return pullData;
}

module.exports = { initPull };
